
""" nombre = input('Ingrese su nombre: ')
print('Su nombre es: ', nombre) """
#Para convertir -> tipo_dato( lo_que_quiero_convertir )
#int('5')
#float('3.14')
#str(10)
#bool()

""" n1 = float(input('Ingrese el primer número: '))
n2 = float(input('Ingrese el segundo número: '))
suma = n1+n2
print(suma)
"""

'''
---------------CALCULADORA----------
1) Sumar
2) Restar
3) Salir
>>>  
'''


def solicitar_numeros():
    n1 = float(input('Ingrese el primer número: '))
    n2 = float(input('Ingrese el segundo número: '))

    return (n1, n2)


def menu():
    mensaje_menu = '------------CALCULADORA-----------\n'
    mensaje_menu += '1) Sumar\n'
    mensaje_menu += '2) Restar\n'
    mensaje_menu+= "3) Multiplicar\n"
    mensaje_menu+= "4) Dividir\n"
    mensaje_menu+= "5) Elevar a la potencia\n"
    mensaje_menu+= "6) Salir\n"
    mensaje_menu+= ">>>"
    
    opcion = 0
    while opcion != 3:
        #Obtener la opción ingresada por el usuario y 
        #convertirla a entero
        opcion = int(input(mensaje_menu))
        #Evaluar la opción ingresada por el usuario
        if opcion == 1:
            n1, n2 = solicitar_numeros()
            print(f'\nLa suma es: {n1+n2}\n')
        elif opcion == 2:
            n1, n2 = solicitar_numeros()
            print(f'\nLa resta es: {n1-n2}\n')
        elif opcion != 3:
            print('Ingrese una opción válida')
        elif opcion == 3:
            n1, n2 = solicitar_numeros()
            print (f'\nLa multiplicación es {n1 * n2}\n')
        elif opcion == 4:
            n1, n2 = solicitar_numeros()
            print (f'\nLa resta es {n1 / n2}\n')
        elif opcion == 5:
            n1, n2 = solicitar_numeros()
            print (f'\nLa potencia es {n1 ** n2}\n')


menu()

'''
Añadir nuevas funcionalidades a la calculadora:
* Multiplicar
* Dividir
* Elevar a la potencia
'''


'''
Calculadora-> Operaciones básicas utilizando while ✔️
Listas compuestas
'''