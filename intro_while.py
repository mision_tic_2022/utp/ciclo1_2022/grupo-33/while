
""" contador = 0

while contador < 10:
    contador += 1
    print(contador)
"""

""" contador = 0
while True:
    contador += 1
    print('-----')
    if contador == 10:
        break
    if contador == 5:
        continue
    print(contador)
 """

contador = 0

while contador <= 1000:
    if contador%2 == 0:
        print(contador)
    contador += 1


flag = True
while flag == True:
    contador += 1
    print(contador)
    if contador >= 2000:
        flag = False

